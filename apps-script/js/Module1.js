
var ss = SpreadsheetApp.getActive();
var Annee_En_Cours = ss.getActiveSpreadsheet().getName().split(" ")[1];

// Pour capturer le log utilisateur
//Private Declare Function WNetGetUser Lib "mpr.dll" Alias "WNetGetUserA"(ByVal lpName As String, ByVal lpUserName As String, lpnLength As Long) As Long

var MyVarBool;
var MyUserName, MyUseSheet;

function prise_en_compte_reunion_formation_par_PO() {

  var ncoJour;
  var nrowCons;
  var Temps_travail_renseigne;

  var sheetTempsPres = ss.getSheetByName(UserForm_TempsPres.mois_concerne).activate();
  //on détermine les coordonnées de la cellule à modifier
  // JOUR
  //ordonnée
  sheetTempsPres.getRange(UserForm_PO.NrDebCal, 1).Select;
  do {
    ActiveCell.Offset(0, 1).Select
  } while (ActiveCell.Value != UserForm_TempsPres.jour_concerne)
  ncoJour = ActiveCell.Column

  //abscisse
  for (var i = UserForm_PO.NrDebCal + 1; i <= UserForm_PO.nrowtotal; i++) {
    if (Cells(i, UserForm_PO.ncolCons).Value = UserForm_PO.collaborateur_concerne) {
      Cells(i, UserForm_PO.ncolCons).Select
      nrowCons = ActiveCell.Row
    }
  }


  Temps_travail_renseigne = sheetTempsPres.getRange(nrowCons, ncoJour).getValue();

  if (!(IsNumeric(Temps_travail_renseigne))) {
    alert("ATTENTION : la journée choisie ne renvoie pas un temps de travail mais un sigle (CP, FOR...). Merci de bien vouloir vérifier")
    //GoTo erreur1
  }

  //on récupère le choix fait sur l'absence liée aux CQP (optionbutton)
  var sheetSuiviCQP = ss.getSheetByName("suivi dispo CQP");
  var lastRow = sheetSuiviCQP.getlastRow();
  var po = userData.A.po;
  var jour = new Date(userData.A.jour);
  var date = jour.getDate();
  var mois = jour.getMonth();
  var annee = jour.getFullYear();
  var absence = userData.A.absence;
  var temps = userData.A.temps;
  var data = [po, date, mois, annee, absence, temps];

  sheetSuiviCQP.getRange(lastRow + 1, 1, 1, data.length.setValues([data]);

  ss.getSheetByName(UserForm_TempsPres.mois_concerne).Select

  alert("Vos modifications ont bien été effectuées!");

  erreur1:

  Unload UserForm_PO
  Unload UserForm_Initial
  Unload UserForm_Message

}