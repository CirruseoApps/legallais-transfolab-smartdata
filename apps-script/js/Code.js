var SCREENS = {
  INI: 'SCINI',
  A1: 'SCA1',
  A2A: 'SCA2A',
  A2B: 'SCA2B',
  B1: 'SCB1',
  C1: 'SCC1',
  A2A_M: 'SCA2A_M',
  MCQP: 'MCQP'
};

var userData = {
  screenChoice: null,
  A: {
    jour: null,
    affectation: null,
    poCollaborateur: null,
    absence: null,
    temps: null,
    superviseur: null,
    choixCollab: [],
    message: null
  },
  B: {
    poCollaborateur: null,
    absences: {
      du: null,
      au: null,
      type: null
    }
  },
  C: {
    poCollaborateur: null,
    nombreHeuresSupp: {
      type: null,
      du: null,
      au: null
    }
  }
};

function getRadioValue(inputName) {
  var value = $('input[name=' + inputName + ']:checked').val();
  return value;
}

function getInputValue(inputName) {
  var value = $('input[name=' + inputName + ']').val();
  return value;
}

function getSelectValues(inputName) {
  var value = $('select[name=' + inputName + ']').val();
  return value;
}

function hideAll() {
  for (var i in SCREENS) {
    $('div[name=' + SCREENS[i]).hide();
  }
}

function hideScreen(screen) {
  $('div[name=' + SCREENS[screen]).hide();
}

function toggleScreen(step) {
  //hideAll();
  $('div[name=' + SCREENS[step]).show();
}

function showHide(screenToShow, screenToHide) {
  toggleScreen(screenToShow);
  hideScreen(screenToHide);
}

function validateScreenInitalize() {
  // Récupérer le choix utilisateur
  var value = getRadioValue('initialize');
  userData.screenChoice = value;
  // Cacher l'écran actuel
  hideScreen('INI');
  // Afficher l'écran correspondant au choix utilisateur et écrire le choix
  value != undefined ? toggleScreen(value) : alert('Sélectionnez une valeur');
}

function validateScreenA1() {
  // Recupérer les entrées utilisateur
  var jour = getInputValue('A1_Jour');
  var affectation = getRadioValue('A1_Affectation');
  // Ecrire les données dans l'objet global
  userData.A.jour = jour;
  userData.A.affectation = affectation;
  // Cacher l'écran actuel
  hideScreen('A1');
  // Afficher l'écran correspondant au choix utilisateur
  if (affectation == 'po') {
    toggleScreen('A2A');
  } else if (affectation == 'equipe') {
    toggleScreen('A2B');
  }
}

function validateScreenA2A() {
  // Récupérer les entrées utilisateur
  var po = getInputValue('A2A_PoCollaborateur');
  var absence = getRadioValue('A2A_Absence');
  var temps = getSelectValues('A2A_Temps');
  userData.A.poCollaborateur = po;
  userData.A.absence = absence;
  userData.A.temps = temps;
  // Afficher l'écran de message
}

function validateScreenA2AM() {
  // Récupérer les entrées utilisateur
  var message = getInputValue('A2AM_Message');
  userData.A.message = message;
}

function validateScreenA2B() {}

function validateScreenB1() {}

function validateScreenC1() {}
