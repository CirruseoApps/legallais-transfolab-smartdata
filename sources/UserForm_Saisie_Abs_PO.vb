

Public Annee_En_Cours As Variant
Private Sub UserForm_Initialize()

Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

'initialisation de la combobox_Abs
    ComboBox_Abs.AddItem "AI - Absence injustifiée non payée"
    ComboBox_Abs.AddItem "AN - Absence autorisée non payée"
    ComboBox_Abs.AddItem "AV - En attente de validation"
    ComboBox_Abs.AddItem "CA - Carence (intérimaire)"
    ComboBox_Abs.AddItem "CP - Congé payé"
    ComboBox_Abs.AddItem "EF - Evénement familial"
    ComboBox_Abs.AddItem "FOR - Formation"
    ComboBox_Abs.AddItem "MA - Maladie"
    ComboBox_Abs.AddItem "MAT - Congé maternité"
    ComboBox_Abs.AddItem "MI - Mission"
    ComboBox_Abs.AddItem "NTP - Ne travaille pas"
    ComboBox_Abs.AddItem "PAR - Parti"
    ComboBox_Abs.AddItem "PAT - Congé paternité"
    ComboBox_Abs.AddItem "RT - RTT"
    ComboBox_Abs.AddItem "GRE - Grève"
    ComboBox_Abs.AddItem "???"

'spécifie la date du jour lors de l'affichage de l'USF
    If Year(Date) = Annee_En_Cours Then
        DTPicker1_Abs.Value = Date
        DTPicker2_Abs.Value = Date
    Else
        If Year(Date) = Annee_En_Cours - 1 Then
            DTPicker1_Abs.Value = DateSerial(Annee_En_Cours, 1, 1)
            DTPicker2_Abs.Value = DateSerial(Annee_En_Cours, 1, 1)
        End If
    End If
    
 End Sub

Private Sub DTPicker1_Abs_Change()
    
'    If Year(DTPicker1_Abs.Value) > Annee_En_Cours Then
'        DTPicker1_Abs.Value = DateSerial(Annee_En_Cours, 12, 31)
'        DTPicker2_Abs.Value = DateSerial(Annee_En_Cours, 12, 31)
'    Else
'        DTPicker2_Abs.Value = DTPicker1_Abs.Value
'    End If

End Sub

Private Sub DTPicker2_Abs_Change()

 '   If DTPicker2_Abs.Value < DTPicker1_Abs.Value Then
 '       DTPicker2_Abs.Value = DTPicker1_Abs.Value
 '   End If

  '  If Year(DTPicker2_Abs.Value) > Annee_En_Cours Then
  '      DTPicker2_Abs.Value = DateSerial(Annee_En_Cours, 12, 31)
  '  End If

End Sub


Private Sub Quit6_Click()

Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

TextPO_Abs.Value = ""
ComboBox_Abs.Value = ""

    If Year(Date) = Annee_En_Cours Then
        DTPicker1_Abs.Value = Date
        DTPicker2_Abs.Value = Date
    Else
        If Year(Date) = Annee_En_Cours - 1 Then
            DTPicker1_Abs.Value = DateSerial(Annee_En_Cours, 1, 1)
            DTPicker2_Abs.Value = DateSerial(Annee_En_Cours, 1, 1)
        End If
    End If
Me.Hide

UserForm_Initial.OptionButton1.Value = False
UserForm_Initial.OptionButton2.Value = False
UserForm_Initial.Show

End Sub


'empêche la saisie d'autre chose que des chiffres
Private Sub TextPO_Abs_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    If InStr("1234567890", Chr(KeyAscii)) = 0 Then KeyAscii = 0
End Sub

Private Sub Valid6_Click()

Dim rngTrouve As Range
Dim text As Variant
Dim PO_Abs As Variant
Dim Mois1_Abs As Variant, Mois2_Abs As Variant, Jour1_Abs As Date, Jour2_Abs As Date, Lib_Mois1_Abs As String, Lib_Mois2_Abs As String
Dim Motif_Abs As String, Collaborateur_Abs As String
Dim Nc_J1 As Long, Nc_J2 As Long
Dim Jour1_Cherch As Date, Jour2_Cherch As Date, NbJ1 As Variant, NbJ2 As Variant
Dim Lib_Motif_Abs As String, NcdernJour As Long
Dim Nb_Mois_Abs As Long, NcPremJour As Long
Dim Lib_Mois_Abs_Intercal As String


If ComboBox_Abs.Value = "" Or TextPO_Abs.Value = "" Then
    MsgBox "Attention! Vous avez omis de saisir des informations demandées. Merci de vérifier."
    GoTo Erreur_Abs
End If


'on récupère le PO choisi
    PO_Abs = Me.TextPO_Abs.Value
    
'On récupère la date de début d'absence et la date de fin
Jour1_Abs = DTPicker1_Abs.Value
Jour2_Abs = DTPicker2_Abs.Value
Mois1_Abs = Month(Jour1_Abs)
Mois2_Abs = Month(Jour2_Abs)

If Mois1_Abs >= 10 Then
    Jour1_Cherch = "01/" & Mois1_Abs & "/" & Annee_En_Cours
Else
    Jour1_Cherch = "01/0" & Mois1_Abs & "/" & Annee_En_Cours
End If

If Mois2_Abs >= 10 Then
    Jour2_Cherch = "01/" & Mois2_Abs & "/" & Annee_En_Cours
Else
    Jour2_Cherch = "01/0" & Mois2_Abs & "/" & Annee_En_Cours
End If

NbJ1 = Jour1_Abs - Jour1_Cherch
NbJ2 = Jour2_Abs - Jour2_Cherch

Select Case Mois1_Abs
    Case 1
        Lib_Mois1_Abs = "Janvier " & Annee_En_Cours
    Case 2
        Lib_Mois1_Abs = "Février " & Annee_En_Cours
    Case 3
        Lib_Mois1_Abs = "Mars " & Annee_En_Cours
    Case 4
        Lib_Mois1_Abs = "Avril " & Annee_En_Cours
    Case 5
        Lib_Mois1_Abs = "Mai " & Annee_En_Cours
    Case 6
        Lib_Mois1_Abs = "Juin " & Annee_En_Cours
    Case 7
        Lib_Mois1_Abs = "Juillet " & Annee_En_Cours
    Case 8
        Lib_Mois1_Abs = "Août " & Annee_En_Cours
    Case 9
        Lib_Mois1_Abs = "Septembre " & Annee_En_Cours
    Case 10
        Lib_Mois1_Abs = "Octobre " & Annee_En_Cours
    Case 11
        Lib_Mois1_Abs = "Novembre " & Annee_En_Cours
    Case 12
        Lib_Mois1_Abs = "Décembre " & Annee_En_Cours
End Select

Select Case Mois2_Abs
    Case 1
        Lib_Mois2_Abs = "Janvier " & Annee_En_Cours
    Case 2
        Lib_Mois2_Abs = "Février " & Annee_En_Cours
    Case 3
        Lib_Mois2_Abs = "Mars " & Annee_En_Cours
    Case 4
        Lib_Mois2_Abs = "Avril " & Annee_En_Cours
    Case 5
        Lib_Mois2_Abs = "Mai " & Annee_En_Cours
    Case 6
        Lib_Mois2_Abs = "Juin " & Annee_En_Cours
    Case 7
        Lib_Mois2_Abs = "Juillet " & Annee_En_Cours
    Case 8
        Lib_Mois2_Abs = "Août " & Annee_En_Cours
    Case 9
        Lib_Mois2_Abs = "Septembre " & Annee_En_Cours
    Case 10
        Lib_Mois2_Abs = "Octobre " & Annee_En_Cours
    Case 11
        Lib_Mois2_Abs = "Novembre " & Annee_En_Cours
    Case 12
        Lib_Mois2_Abs = "Décembre " & Annee_En_Cours
End Select


'On récupère le motif d'absence
Motif_Abs = ComboBox_Abs.Value

Select Case Motif_Abs
    Case "AI - Absence injustifiée non payée"
        Lib_Motif_Abs = "AI"
    Case "AN - Absence autorisée non payée"
        Lib_Motif_Abs = "AN"
    Case "AV - En attente de validation"
        Lib_Motif_Abs = "AV"
    Case "CA - Carence (intérimaire)"
        Lib_Motif_Abs = "CA"
    Case "CP - Congé payé"
        Lib_Motif_Abs = "CP"
    Case "EF - Evénement familial"
        Lib_Motif_Abs = "EF"
    Case "FOR - Formation"
        Lib_Motif_Abs = "FOR"
    Case "MA - Maladie"
        Lib_Motif_Abs = "MA"
    Case "MAT - Congé maternité"
        Lib_Motif_Abs = "MAT"
    Case "MI - Mission"
        Lib_Motif_Abs = "MI"
    Case "NTP - Ne travaille pas"
        Lib_Motif_Abs = "NTP"
    Case "PAR - Parti"
        Lib_Motif_Abs = "PAR"
    Case "PAT - Congé paternité"
        Lib_Motif_Abs = "PAT"
    Case "RT - RTT"
        Lib_Motif_Abs = "RT"
    Case "GRE - Grève"
        Lib_Motif_Abs = "GRE"
    Case "???"
        Lib_Motif_Abs = "???"
End Select

Nb_Mois_Abs = Mois2_Abs - Mois1_Abs + 1


If Nb_Mois_Abs = 1 Then
    ActiveWorkbook.Activate
        Sheets(Lib_Mois1_Abs).Select
            Cells.Select
                Selection.Find(what:="Matricule").Activate
                    ncoMatricule = ActiveCell.Column
                    ncolCons = ActiveCell.Column + 1
                    NrDebCal = ActiveCell.Row
                    nrowtotal = ActiveSheet.UsedRange.Rows.Count

            Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                Selection.Find(what:=DateValue(Jour1_Cherch), LookIn:=xlFormulas).Activate
                    Nc_J1 = ActiveCell.Column + NbJ1

            Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                Selection.Find(what:=DateValue(Jour2_Cherch), LookIn:=xlFormulas).Activate
                    Nc_J2 = ActiveCell.Column + NbJ2

                    
        ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_Abs, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_Abs.Value = ""
                    GoTo Erreur_Abs
                End If
            
            Set rngTrouve = Nothing
        
        'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_Abs).Select
                    Collaborateur_Abs = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch = ActiveCell.Row
                    
        
        For i = Nc_J1 To Nc_J2
            If Cells(Nr_Cherch, i).Value = "" Then
                Cells(Nr_Cherch, i).Value = ""
            Else
                Cells(Nr_Cherch, i).Select
                Cells(Nr_Cherch, i).Value = Lib_Motif_Abs
            End If
        Next i
        
Else
    If Nb_Mois_Abs = 2 Then
            ActiveWorkbook.Activate
            
            ''''''''''''''
            'on renseigne le mois 1 (de début d'absence)
                Sheets(Lib_Mois1_Abs).Select
                    Cells.Select
                        Selection.Find(what:="Matricule", lookat:=xlWhole).Activate
                            ncoMatricule = ActiveCell.Column
                            ncolCons = ActiveCell.Column + 1
                            NrDebCal = ActiveCell.Row
                            nrowtotal = ActiveSheet.UsedRange.Rows.Count
                    Cells.Select
                        Selection.Find(what:="Nb Jours Travaillés", lookat:=xlWhole).Select
                            NcdernJour = ActiveCell.Column - 1

                    Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                        Selection.Find(what:=DateValue(Jour1_Cherch), LookIn:=xlFormulas).Activate
                            Nc_J1 = ActiveCell.Column + NbJ1
        
                    
            ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_Abs, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_Abs.Value = ""
                    GoTo Erreur_Abs
                End If
            
            Set rngTrouve = Nothing
        
            'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_Abs).Select
                    Collaborateur_Abs = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch = ActiveCell.Row
                    
        
                For i = Nc_J1 To NcdernJour
                    If Cells(Nr_Cherch, i).Value = "" Then
                        Cells(Nr_Cherch, i).Value = ""
                    Else
                        Cells(Nr_Cherch, i).Select
                        Cells(Nr_Cherch, i).Value = Lib_Motif_Abs
                    End If
                Next i
        
        ''''''''''''''
            'on renseigne le mois 2 (de fin d'absence)
                Sheets(Lib_Mois2_Abs).Select
                    Cells.Select
                        Selection.Find(what:="Matricule", lookat:=xlWhole).Activate
                            ncoMatricule = ActiveCell.Column
                            ncolCons = ActiveCell.Column + 1
                            NrDebCal = ActiveCell.Row
                            nrowtotal = ActiveSheet.UsedRange.Rows.Count
                    Cells.Select
                        Selection.Find(what:="FONCTION", lookat:=xlWhole).Select
                            NcPremJour = ActiveCell.Column + 1

                    Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                        Selection.Find(what:=DateValue(Jour2_Cherch), LookIn:=xlFormulas).Activate
                            Nc_J2 = ActiveCell.Column + NbJ2
        
                    
        ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_Abs, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_Abs.Value = ""
                    GoTo Erreur_Abs
                End If
            
            Set rngTrouve = Nothing
        
        'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_Abs).Select
                    Collaborateur_Abs = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch = ActiveCell.Row
                    
        
        For i = NcPremJour To Nc_J2
            If Cells(Nr_Cherch, i).Value = "" Then
                Cells(Nr_Cherch, i).Value = ""
            Else
                Cells(Nr_Cherch, i).Select
                Cells(Nr_Cherch, i).Value = Lib_Motif_Abs
            End If
        Next i

    Else
        If Nb_Mois_Abs > 2 Then
        
            ActiveWorkbook.Activate
            
            ''''''''''''''
            'on renseigne le mois 1 (de début d'absence)
                Sheets(Lib_Mois1_Abs).Select
                    Cells.Select
                        Selection.Find(what:="Matricule", lookat:=xlWhole).Activate
                            ncoMatricule = ActiveCell.Column
                            ncolCons = ActiveCell.Column + 1
                            NrDebCal = ActiveCell.Row
                            nrowtotal = ActiveSheet.UsedRange.Rows.Count
                    Cells.Select
                        Selection.Find(what:="Nb Jours Travaillés", lookat:=xlWhole).Select
                            NcdernJour = ActiveCell.Column - 1

                    Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                        Selection.Find(what:=DateValue(Jour1_Cherch), LookIn:=xlFormulas).Activate
                            Nc_J1 = ActiveCell.Column + NbJ1
        
                    
            ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_Abs, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_Abs.Value = ""
                    GoTo Erreur_Abs
                End If
            
            Set rngTrouve = Nothing
        
            'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_Abs).Select
                    Collaborateur_Abs = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch = ActiveCell.Row
                    
        
                For i = Nc_J1 To NcdernJour
                    If Cells(Nr_Cherch, i).Value = "" Then
                        Cells(Nr_Cherch, i).Value = ""
                    Else
                        Cells(Nr_Cherch, i).Select
                        Cells(Nr_Cherch, i).Value = Lib_Motif_Abs
                    End If
                Next i
        
        ''''''''''''''
            'on renseigne le dernier mois (de fin d'absence)
                Sheets(Lib_Mois2_Abs).Select
                    Cells.Select
                        Selection.Find(what:="Matricule", lookat:=xlWhole).Activate
                            ncoMatricule = ActiveCell.Column
                            ncolCons = ActiveCell.Column + 1
                            NrDebCal = ActiveCell.Row
                            nrowtotal = ActiveSheet.UsedRange.Rows.Count
                    Cells.Select
                        Selection.Find(what:="FONCTION", lookat:=xlWhole).Select
                            NcPremJour = ActiveCell.Column + 1

                    Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                        Selection.Find(what:=DateValue(Jour2_Cherch), LookIn:=xlFormulas).Activate
                            Nc_J2 = ActiveCell.Column + NbJ2
        
                    
        ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_Abs, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_Abs.Value = ""
                    GoTo Erreur_Abs
                End If
            
            Set rngTrouve = Nothing
        
        'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_Abs).Select
                    Collaborateur_Abs = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch = ActiveCell.Row
                    
        
        For i = NcPremJour To Nc_J2
            If Cells(Nr_Cherch, i).Value = "" Then
                Cells(Nr_Cherch, i).Value = ""
            Else
                Cells(Nr_Cherch, i).Select
                Cells(Nr_Cherch, i).Value = Lib_Motif_Abs
            End If
        Next i

        ''''''''''''''
            'on renseigne les mois intercalés
            For j = Mois1_Abs + 1 To Mois2_Abs - 1
                
                Select Case j
                    Case 1
                        Lib_Mois_Abs_Intercal = "Janvier " & Annee_En_Cours
                    Case 2
                        Lib_Mois_Abs_Intercal = "Février " & Annee_En_Cours
                    Case 3
                        Lib_Mois_Abs_Intercal = "Mars " & Annee_En_Cours
                    Case 4
                        Lib_Mois_Abs_Intercal = "Avril " & Annee_En_Cours
                    Case 5
                        Lib_Mois_Abs_Intercal = "Mai " & Annee_En_Cours
                    Case 6
                        Lib_Mois_Abs_Intercal = "Juin " & Annee_En_Cours
                    Case 7
                        Lib_Mois_Abs_Intercal = "Juillet " & Annee_En_Cours
                    Case 8
                        Lib_Mois_Abs_Intercal = "Août " & Annee_En_Cours
                    Case 9
                        Lib_Mois_Abs_Intercal = "Septembre " & Annee_En_Cours
                    Case 10
                        Lib_Mois_Abs_Intercal = "Octobre " & Annee_En_Cours
                    Case 11
                        Lib_Mois_Abs_Intercal = "Novembre " & Annee_En_Cours
                    Case 12
                        Lib_Mois_Abs_Intercal = "Décembre " & Annee_En_Cours
                End Select

                
                
                Sheets(Lib_Mois_Abs_Intercal).Select
                    Cells.Select
                        Selection.Find(what:="Matricule", lookat:=xlWhole).Activate
                            ncoMatricule = ActiveCell.Column
                            ncolCons = ActiveCell.Column + 1
                            NrDebCal = ActiveCell.Row
                            nrowtotal = ActiveSheet.UsedRange.Rows.Count
                    Cells.Select
                        Selection.Find(what:="FONCTION", lookat:=xlWhole).Select
                            NcPremJour = ActiveCell.Column + 1

                    Cells.Select
                        Selection.Find(what:="Nb Jours Travaillés", lookat:=xlWhole).Select
                            NcdernJour = ActiveCell.Column - 1
        
                    
        ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_Abs, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_Abs.Value = ""
                    GoTo Erreur_Abs
                End If
            
            Set rngTrouve = Nothing
        
        'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_Abs).Select
                    Collaborateur_Abs = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch = ActiveCell.Row
                    
        
                For i = NcPremJour To NcdernJour
                    If Cells(Nr_Cherch, i).Value = "" Then
                        Cells(Nr_Cherch, i).Value = ""
                    Else
                        Cells(Nr_Cherch, i).Select
                        Cells(Nr_Cherch, i).Value = Lib_Motif_Abs
                    End If
                Next i

            Next j

        End If
    End If
End If

Sheets(Lib_Mois1_Abs).Select


'remise à vide de l'userform
TextPO_Abs.Value = ""
ComboBox_Abs.Value = ""

    If Year(Date) = Annee_En_Cours Then
        DTPicker1_Abs.Value = Date
        DTPicker2_Abs.Value = Date
    Else
        DTPicker1_Abs.Value = DateSerial(Annee_En_Cours, 1, 1)
        DTPicker2_Abs.Value = DateSerial(Annee_En_Cours, 1, 1)
    End If


UserForm_Saisie_Abs_PO.Hide



Erreur_Abs:
End Sub
