

Public jour_concerne As String
Public mois_concerne As String
Public Annee_En_Cours As Variant

Private Sub UserForm_Initialize()
    Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

    'spécifie la date du jour lors de l'affichage de l'USF
    If Year(Date) = Annee_En_Cours Then
        DTPicker1.Value = Date
    Else
        If Year(Date) = Annee_En_Cours - 1 Then
            DTPicker1.Value = DateSerial(Annee_En_Cours, 1, 1)
        End If
    End If
End Sub

Private Sub DTPicker1_Change()
    
'    If Year(DTPicker1.Value) > Annee_En_Cours Then
'        DTPicker1.Value = DateSerial(Annee_En_Cours, 12, 31)
'    End If

End Sub

Private Sub Quit1_Click()
    DTPicker1.Value = Date
    OptionButton1.Value = False
    OptionButton2.Value = False
    Unload Me
    UserForm_Initial.OptionButton1.Value = False
    UserForm_Initial.OptionButton2.Value = False
    UserForm_Initial.Show
    
End Sub


Private Sub Valid1_Click()
Application.ScreenUpdating = False

Dim mois As Integer 'N° du mois de la date choisie

Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

'''''''''''''''''''''''''''''''''
'travail sur la date récupérée
'''''''''''''''''''''''''''''''''

'on récupère le jour choisi
    jour_concerne = Me.DTPicker1.Value
    
'on détermine les variables nécessaires pour localiser l'onglet choisi
        mois = Month(jour_concerne)
    
        Select Case mois
            Case "1"
                mois_concerne = "Janvier " & Annee_En_Cours
            Case "2"
                mois_concerne = "Février " & Annee_En_Cours
            Case "3"
                mois_concerne = "Mars " & Annee_En_Cours
            Case "4"
                mois_concerne = "Avril " & Annee_En_Cours
            Case "5"
                mois_concerne = "Mai " & Annee_En_Cours
            Case "6"
                mois_concerne = "Juin " & Annee_En_Cours
            Case "7"
                mois_concerne = "Juillet " & Annee_En_Cours
            Case "8"
                mois_concerne = "Août " & Annee_En_Cours
            Case "9"
                mois_concerne = "Septembre " & Annee_En_Cours
            Case "10"
                mois_concerne = "Octobre " & Annee_En_Cours
            Case "11"
                mois_concerne = "Novembre " & Annee_En_Cours
            Case "12"
                mois_concerne = "Décembre " & Annee_En_Cours
        End Select
        
        
'''''''''''''''''''''''''''''''''
'travail sur le choix récupéré (par PO ou par équipe)
'''''''''''''''''''''''''''''''''

'contrôle qu'une case a été cochée
Dim Cpt As Byte, i As Byte
For i = 1 To 2
    Cpt = Cpt - (Controls("OptionButton" & i).Value = False)
Next i
If Cpt = 2 Then
    Me.Hide
    MsgBox ("Veuillez cocher une proposition d'affectation (par PO ou par équipe)")
    Me.Show
    Exit Sub
End If

If Year(Date) = Annee_En_Cours Then
    If OptionButton1.Value = True Then
        DTPicker1 = Date
        OptionButton1.Value = False
        OptionButton2.Value = False
        Me.Hide
        UserForm_PO.Show
    Else
        DTPicker1 = Date
        OptionButton1.Value = False
        OptionButton2.Value = False
        Me.Hide
        UserForm_Equipe.Show
    End If
Else
    If OptionButton1.Value = True Then
        DTPicker1 = DateSerial(Annee_En_Cours, 1, 1)
        OptionButton1.Value = False
        OptionButton2.Value = False
        Me.Hide
        UserForm_PO.Show
    Else
        DTPicker1 = DateSerial(Annee_En_Cours, 1, 1)
        OptionButton1.Value = False
        OptionButton2.Value = False
        Me.Hide
        UserForm_Equipe.Show
    End If
End If


End Sub
