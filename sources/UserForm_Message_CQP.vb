

Private Sub Quit5_Click()
    Me.TextBox1.Value = ""
    Unload Me
    UserForm_PO.Show
End Sub


Private Sub UserForm_Initialize()

Dim Annee_En_Cours As Variant

Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

            'on comptabilise les heures de tutorat/accompagnement/formation occasionnelle réalisée
            Windows("Congés " & Annee_En_Cours & ".xlsm").Activate
                Sheets("suivi dispo CQP").Select
                    If Range("A2").Value <> "" Then
                        plage_PO = Range(Cells(2, 1), Cells(ActiveSheet.UsedRange.Rows.Count, 1)).Address
                        plage_Mois = Range(Cells(2, 4), Cells(ActiveSheet.UsedRange.Rows.Count, 4)).Address
                        plage_Annee = Range(Cells(2, 5), Cells(ActiveSheet.UsedRange.Rows.Count, 5)).Address
                        plage_CQP = Range(Cells(2, 6), Cells(ActiveSheet.UsedRange.Rows.Count, 6)).Address
                        plage_Tps = Range(Cells(2, 7), Cells(ActiveSheet.UsedRange.Rows.Count, 7)).Address
                        
                        mois = Month(UserForm_TempsPres.jour_concerne)
                        Annee = Year(UserForm_TempsPres.jour_concerne)
                        
                            temps_tutorat_mensuel = Evaluate("=SUMPRODUCT((" & plage_PO & "=" & UserForm_PO.PO_concerne & ")*(" & plage_Mois & "=" & mois & ")*(" & plage_Annee & "=" & Annee & ")*(" & plage_CQP & "=""Tutorat"")*(" & plage_Tps & "))") / 60
                            temps_accompagnement = Evaluate("=SUMPRODUCT((" & plage_PO & "=" & UserForm_PO.PO_concerne & ")*(" & plage_CQP & "=""Accompagnement"")*(" & plage_Tps & "))") / 60
                            temps_mensuel_Formation_occasionnelle = Evaluate("=SUMPRODUCT((" & plage_PO & "=" & UserForm_PO.PO_concerne & ")*(" & plage_Mois & "=" & mois & ")*(" & plage_Annee & "=" & Annee & ")*(" & plage_CQP & "=""Formateur occasionnel"")*(" & plage_Tps & "))") / 60
                    End If


    Me.TextBox1.Value = "Avant cette saisie, " & UserForm_PO.collaborateur_concerne & " comptabilisait : " & Chr(10) & "Temps de Tutorat du mois de " & UserForm_TempsPres.mois_concerne & " : " & temps_tutorat_mensuel & " heures" & Chr(10) & "Temps de formation occasionnelle du mois de " & UserForm_TempsPres.mois_concerne & " : " & temps_mensuel_Formation_occasionnelle & " heures" & Chr(10) & "Temps d'accompagnement total depuis le début: " & temps_accompagnement & " heures"
End Sub

Private Sub Valid5_Click()
    Unload Me
    Call prise_en_compte_reunion_formation_par_PO
End Sub

