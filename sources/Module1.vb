
Public Annee_En_Cours As Variant

' Pour capturer le log utilisateur
Private Declare Function WNetGetUser Lib "mpr.dll" Alias "WNetGetUserA" (ByVal lpName As String, ByVal lpUserName As String, lpnLength As Long) As Long
 
Dim MyVarBool As Boolean
Dim MySheet As Worksheet
Dim MyUserName, MyUseSheet As Variant
'-------------------------------------------------------------------------------------------------------------
' Fonction de récupération du FL de l'utilisateur
'-------------------------------------------------------------------------------------------------------------
Public Function GetUserName()
    Const lpnLength As Integer = 255
    Dim status As Integer
    Dim lpName, lpUserName As String
    
    Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))
    
    'Utilisation de la fonction WNetGetUser pour récupérer le nom de l'utilisateur du système
    lpUserName = Space$(lpnLength + 1)
    status = WNetGetUser(lpName, lpUserName, lpnLength)
    'Gestion d'erreur de la récupération
    If status = NoError Then
        lpUserName = Left$(lpUserName, InStr(lpUserName, Chr(0)) - 1)
    Else
        MsgBox "Impossible d'obtenir le PO."
        End
    End If
    'Résultat de la fonction
    GetUserName = UCase(lpUserName)
    
End Function


Sub Supp_Cbar()

    Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

On Error Resume Next 'gestion de l'erreur si la barre n'existe pas
    CommandBars("MaBarreCongés" & Annee_En_Cours & "").Delete

End Sub

Sub Creation_Cbar()
On Error Resume Next
Dim Cbar As CommandBar

    Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))


Set Cbar = CommandBars.Add("MaBarreCongés" & Annee_En_Cours & "", msoBarTop, Temporary:=True)
    'les propriétés de la barre
    With Cbar
        .Visible = True
        .Protection = msobadrNocustomize + msoBarNoMove 'empêche d'ajouter un bouton à la barre de menu et empêche de déplacer la barre de menus
    End With
    
    'les contrôles
    With CommandBars("MaBarreCongés" & Annee_En_Cours & "").Controls.Add(msoControlButton)
        .FaceId = "29"
        .OnAction = "lancement_userform"
        .TooltipText = "Pour tenir compte de réunions, formation (...) dans le temps de présence"
        .BeginGroup = True
    End With

End Sub

Sub lancement_userform()
    UserForm_Initial.Show
End Sub

Sub prise_en_compte_reunion_formation_par_PO()

Dim ncoJour As Integer 'N° de la colonne du jour concerné
Dim nrowCons As Integer 'N° de la ligne du conseiller concerné
Dim Temps_travail_renseigne As Variant 'temps de travail initilement renseigné dans le fichier congés
Dim MySheet As Worksheet

    Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))


Windows("Congés " & Annee_En_Cours & ".xlsm").Activate

    For Each MySheet In ActiveWorkbook.Worksheets
        MySheet.Activate
        MySheet.Unprotect Password:="ziggy"
    Next MySheet


    Sheets(UserForm_TempsPres.mois_concerne).Select
    'on détermine les coordonnées de la cellule à modifier
        'ordonnée
        Cells(UserForm_PO.NrDebCal, 1).Select
            Do While ActiveCell.Value <> UserForm_TempsPres.jour_concerne
                ActiveCell.Offset(0, 1).Select
            Loop
            ncoJour = ActiveCell.Column
            
        'abscisse
        For i = UserForm_PO.NrDebCal + 1 To UserForm_PO.nrowtotal
            If Cells(i, UserForm_PO.ncolCons).Value = UserForm_PO.collaborateur_concerne Then
                Cells(i, UserForm_PO.ncolCons).Select
                nrowCons = ActiveCell.Row
            End If
        Next i
        
        Temps_travail_renseigne = Cells(nrowCons, ncoJour).Value
        
        If Not IsNumeric(Temps_travail_renseigne) Then
            MsgBox "ATTENTION : la journée choisie ne renvoie pas un temps de travail mais un sigle (CP, FOR...). Merci de bien vouloir vérifier"
            GoTo erreur1
        End If
        
                    Cells(nrowCons, ncoJour).Select
                    
                    'on doit retirer la validation des données pour saisir notre nouvelle donnée puis la remettre ensuite
                    With Selection.Validation
                        .Delete
                        .Add Type:=xlValidateInputOnly, AlertStyle:=xlValidAlertStop, Operator:=xlBetween
                        .IgnoreBlank = True
                        .InCellDropdown = True
                        .InputTitle = ""
                        .ErrorTitle = ""
                        .InputMessage = ""
                        .ErrorMessage = ""
                        .ShowInput = True
                        .ShowError = True
                    End With
                ActiveCell.Value = Round((((Temps_travail_renseigne * 7.4) - (UserForm_PO.temps_a_impacter / 60)) / 7.4), 2)
                    With Selection.Validation
                        .Delete
                        .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, Operator:=xlBetween, Formula1:="=CODES_X"
                        .IgnoreBlank = True
                        .InCellDropdown = True
                        .InputTitle = ""
                        .ErrorTitle = ""
                        .InputMessage = ""
                        .ErrorMessage = ""
                        .ShowInput = True
                        .ShowError = True
                    End With
            
            
            'on récupère le choix fait sur l'absence liée aux CQP (optionbutton)
                If UserForm_PO.OptionButton1.Value = True Then
                    Sheets("suivi dispo CQP").Select
                        Range("A1").Select
                        Do While ActiveCell.Value <> 0
                            ActiveCell.Offset(1, 0).Select
                        Loop
                            ActiveCell.Value = UserForm_PO.PO_concerne
                            ActiveCell.Offset(0, 1).Value = UserForm_PO.collaborateur_concerne
                            ActiveCell.Offset(0, 2).Value = CDate(UserForm_TempsPres.jour_concerne)
                            ActiveCell.Offset(0, 3).Value = Month(UserForm_TempsPres.jour_concerne)
                            ActiveCell.Offset(0, 4).Value = Year(UserForm_TempsPres.jour_concerne)
                            ActiveCell.Offset(0, 5).Value = "Tutorat"
                            ActiveCell.Offset(0, 6).Value = UserForm_PO.temps_a_impacter
                Else
                    If UserForm_PO.OptionButton2.Value = True Then
                    Sheets("suivi dispo CQP").Select
                        Range("A1").Select
                        Do While ActiveCell.Value <> 0
                            ActiveCell.Offset(1, 0).Select
                        Loop
                            ActiveCell.Value = UserForm_PO.PO_concerne
                            ActiveCell.Offset(0, 1).Value = UserForm_PO.collaborateur_concerne
                            ActiveCell.Offset(0, 2).Value = CDate(UserForm_TempsPres.jour_concerne)
                            ActiveCell.Offset(0, 3).Value = Month(UserForm_TempsPres.jour_concerne)
                            ActiveCell.Offset(0, 4).Value = Year(UserForm_TempsPres.jour_concerne)
                            ActiveCell.Offset(0, 5).Value = "Accompagnement"
                            ActiveCell.Offset(0, 6).Value = UserForm_PO.temps_a_impacter
                    Else
                        If UserForm_PO.OptionButton3.Value = True Then
                            Sheets("suivi dispo CQP").Select
                                Range("A1").Select
                                Do While ActiveCell.Value <> 0
                                    ActiveCell.Offset(1, 0).Select
                                Loop
                                    ActiveCell.Value = UserForm_PO.PO_concerne
                                    ActiveCell.Offset(0, 1).Value = UserForm_PO.collaborateur_concerne
                                    ActiveCell.Offset(0, 2).Value = CDate(UserForm_TempsPres.jour_concerne)
                                    ActiveCell.Offset(0, 3).Value = Month(UserForm_TempsPres.jour_concerne)
                                    ActiveCell.Offset(0, 4).Value = Year(UserForm_TempsPres.jour_concerne)
                                    ActiveCell.Offset(0, 5).Value = "Formateur occasionnel"
                                    ActiveCell.Offset(0, 6).Value = UserForm_PO.temps_a_impacter
                        End If
                    End If
                End If
                
    For Each MySheet In ActiveWorkbook.Worksheets
        MySheet.Activate
        MySheet.Protect DrawingObjects:=False, Contents:=True, Scenarios:= _
        True, AllowFormattingCells:=True, AllowFormattingColumns:=True, _
        AllowFormattingRows:=True, AllowSorting:=True, AllowFiltering:=True
    Next MySheet
    
    Sheets(UserForm_TempsPres.mois_concerne).Select



            MsgBox "Vos modifications ont bien été effectuées!"

erreur1:

Unload UserForm_PO
Unload UserForm_Initial
Unload UserForm_Message

End Sub

Sub Déprotection_classeur()

Dim MySheet As Worksheet

    For Each MySheet In ActiveWorkbook.Worksheets
        MySheet.Activate
        MySheet.Unprotect Password:="ziggy"
    Next MySheet
    
End Sub

Sub Protection_classeur()

Dim MySheet As Worksheet

    For Each MySheet In ActiveWorkbook.Worksheets
        MySheet.Activate
        MySheet.Protect Password:="ziggy", DrawingObjects:=False, Contents:=True, Scenarios:= _
        True, AllowFormattingCells:=True, AllowFormattingColumns:=True, _
        AllowFormattingRows:=True, AllowSorting:=True, AllowFiltering:=True
    Next MySheet
    
End Sub






