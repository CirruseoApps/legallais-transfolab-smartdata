
Public ncolEquipe As Integer    'N° de la colonne EQUIPE
Public ncoMatricule As Integer  'N° de la colonne Matricule
Public ncolCons As Integer      'N° de la colonne Conseiller
Public NrDebCal As Integer      'N° de la 1ere ligne du calendrier
Public nrowtotal As Integer     'Nombre de lignes du calendrier
Public ncolSup As Integer       'N° de la colonne SUPERVISEUR
Public temps_a_impacter As Variant
Public Nom_Sup As String
Public Annee_En_Cous As Variant



Private Sub Quit3_Click()
    CheckBox1.Value = False
    ComboBox_temps_reunion.Value = ""
    ComboBox_Sup.Value = ""
    Unload Me
    
    UserForm_TempsPres.DTPicker1 = Date
    UserForm_TempsPres.OptionButton1.Value = False
    UserForm_TempsPres.OptionButton2.Value = False
    UserForm_TempsPres.Show
End Sub


Private Sub CheckBox1_Click()

Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

    
    If ComboBox_Sup.Value = "" And CheckBox1.Value = True Then
        MsgBox "Attention, vous devez d'abord choisir un superviseur!"
        CheckBox1.Value = False
    End If
    
    
        
    If CheckBox1.Value = True Then
        Windows("Congés " & Annee_En_Cours & ".xlsm").Activate
            Sheets(UserForm_TempsPres.mois_concerne).Select
                t = -1
                For i = NrDebCal + 1 To nrowtotal
                    If Cells(i, ncolSup).Value = Me.ComboBox_Sup.Value Then
                        t = t + 1
                            ListBox_PO.Selected(t) = True
                    End If
                Next i
    Else
        Windows("Congés " & Annee_En_Cours & ".xlsm").Activate
            Sheets(UserForm_TempsPres.mois_concerne).Select
                t = -1
                For i = NrDebCal + 1 To nrowtotal
                    If Cells(i, ncolSup).Value = Me.ComboBox_Sup.Value Then
                        t = t + 1
                            ListBox_PO.Selected(t) = False
                    End If
                Next i
    End If

    
    
End Sub



Private Sub UserForm_Initialize()

Dim n As Integer
Dim nbsup As Integer
Dim i, j As Integer

Dim Superviseur() As String  'liste des différents superviseurs
Dim Annee_En_Cours As Variant

Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

'initialisation de la combobox temps_reunion
    Me.ComboBox_temps_reunion.AddItem "15 min"
    Me.ComboBox_temps_reunion.AddItem "20 min"
    Me.ComboBox_temps_reunion.AddItem "30 min"
    Me.ComboBox_temps_reunion.AddItem "45 min"
    Me.ComboBox_temps_reunion.AddItem "1h"
    Me.ComboBox_temps_reunion.AddItem "1h15"
    Me.ComboBox_temps_reunion.AddItem "1h30"
    Me.ComboBox_temps_reunion.AddItem "1h45"
    Me.ComboBox_temps_reunion.AddItem "2h"
    Me.ComboBox_temps_reunion.AddItem "2h15"
    Me.ComboBox_temps_reunion.AddItem "2h30"
    Me.ComboBox_temps_reunion.AddItem "2h45"
    Me.ComboBox_temps_reunion.AddItem "3h"
    Me.ComboBox_temps_reunion.AddItem "3h15"
    Me.ComboBox_temps_reunion.AddItem "3h30"
    Me.ComboBox_temps_reunion.AddItem "3h45"
    Me.ComboBox_temps_reunion.AddItem "4h"
    Me.ComboBox_temps_reunion.AddItem "4h15"
    Me.ComboBox_temps_reunion.AddItem "4h30"
    Me.ComboBox_temps_reunion.AddItem "4h45"
    Me.ComboBox_temps_reunion.AddItem "5h"
    

'initialisation des choix possibles superviseurs
 ComboBox_Sup.Clear
        
    Windows("Congés " & Annee_En_Cours & ".xlsm").Activate
    
        Sheets(UserForm_TempsPres.mois_concerne).Select
            Cells.Select
                Selection.Find(what:="Matricule").Activate
                    ncoMatricule = ActiveCell.Column
                    ncolCons = ActiveCell.Column + 1
                    NrDebCal = ActiveCell.Row
                    nrowtotal = ActiveSheet.UsedRange.Rows.Count
            Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                Selection.Find(what:="SUPERVISEUR").Activate
                    ncolSup = ActiveCell.Column
            Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                Selection.Find(what:="EQUIPE").Activate
                    ncolEquipe = ActiveCell.Column
                    
            n = 1
            nbsup = 0
            ReDim Superviseur(20)
                    
            For i = NrDebCal + 1 To nrowtotal
                If Cells(i, ncolEquipe).Value <> "Managers" Then
                    If Cells(i, ncolSup).Value <> Cells(i - 1, ncolSup).Value Then
                        For j = 1 To n
                            If Cells(i, ncolSup).Value <> Superviseur(j) Then
                                GoTo suite3
                            Else
                                GoTo suite4
                            End If
suite3:
                        Next j
                            nbsup = nbsup + 1
                            Superviseur(nbsup) = Cells(i, ncolSup).Value
                            n = nbsup
                            
suite4:
                    End If
                End If
            Next i
            
            For i = 1 To nbsup
                Me.ComboBox_Sup.AddItem Superviseur(i)
            Next i
    
End Sub


Private Sub ComboBox_Sup_Change()

Dim cell As Range
Dim n As Integer
Dim nbCons As Integer
Dim i, j As Integer
Dim Nom_Sup As String
Dim collaborateur() As String  'liste des différents collaborateurs

Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))


Nom_Sup = ComboBox_Sup.Value

    ListBox_PO.Clear
        
    Windows("Congés " & Annee_En_Cours & ".xlsm").Activate
        Sheets(UserForm_TempsPres.mois_concerne).Select
        
            n = 0
            nbCons = 0
            ReDim collaborateur(100)
                    
            For i = NrDebCal + 1 To nrowtotal
                If Cells(i, ncolSup).Value = Nom_Sup Then
                    If Cells(i, ncolCons).Value <> Cells(i - 1, ncolCons).Value Then
                        n = n + 1
                        For j = 1 To n
                            If Cells(i, ncolCons).Value <> collaborateur(j) Then
                                GoTo suite1
                            Else
                                GoTo suite2
                            End If
suite1:
                        Next j
                            nbCons = nbCons + 1
                            collaborateur(nbCons) = Cells(i, ncolCons).Value
                            
suite2:
                    End If
                End If
            Next i
            
            For i = 1 To nbCons
                Me.ListBox_PO.AddItem collaborateur(i)
            Next i


End Sub



Private Sub Valid3_Click()

Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))


Dim ncolselec As Integer
ncolselec = 6
Dim i, j, test As Integer

If ComboBox_Sup.Value = "" Or ComboBox_temps_reunion.Value = "" Then
    MsgBox "Attention! Vous avez omis de saisir des informations demandées. Merci de vérifier."
    GoTo erreur1
Else
    For i = 0 To ListBox_PO.ListCount - 1
        If ListBox_PO.Selected(i) = True Then
            GoTo suite5
        End If
    Next i
        MsgBox "Attention! Vous avez omis de saisir des informations demandées. Merci de vérifier."
        GoTo erreur1
suite5:
End If

Me.Hide

'on récupère le temps de réunion choisi
    Select Case Me.ComboBox_temps_reunion.Value
        Case "15 min"
            temps_a_impacter = 15
        Case "20 min"
            temps_a_impacter = 20
        Case "30 min"
            temps_a_impacter = 30
        Case "45 min"
            temps_a_impacter = 45
        Case "1h"
            temps_a_impacter = 60
        Case "1h15"
            temps_a_impacter = 75
        Case "1h30"
            temps_a_impacter = 90
        Case "1h45"
            temps_a_impacter = 105
        Case "2h"
            temps_a_impacter = 120
        Case "2h15"
            temps_a_impacter = 135
        Case "2h30"
            temps_a_impacter = 150
        Case "2h45"
            temps_a_impacter = 165
        Case "3h"
            temps_a_impacter = 180
        Case "3h15"
            temps_a_impacter = 195
        Case "3h30"
            temps_a_impacter = 210
        Case "3h45"
            temps_a_impacter = 225
        Case "4h"
            temps_a_impacter = 240
        Case "4h15 min"
            temps_a_impacter = 255
        Case "4h30"
            temps_a_impacter = 270
        Case "4h45"
            temps_a_impacter = 285
        Case "5h"
            temps_a_impacter = 300
    End Select


'calcul de la nouvelle valeur de présence
Dim ncoJour As Integer 'N° de la colonne du jour concerné
Dim Temps_travail_renseigne As Variant 'temps de travail initilement renseigné dans le fichier congés

Windows("Congés " & Annee_En_Cours & ".xlsm").Activate

    For Each MySheet In ActiveWorkbook.Worksheets
        MySheet.Activate
        MySheet.Unprotect Password:="ziggy"
    Next MySheet


    Sheets(UserForm_TempsPres.mois_concerne).Select
        'on détermine l'ordonnée des cellules à modifier
        Cells(NrDebCal, 1).Select
            Do While ActiveCell.Value <> UserForm_TempsPres.jour_concerne
                ActiveCell.Offset(0, 1).Select
            Loop
            ncoJour = ActiveCell.Column

            
    For i = 0 To ListBox_PO.ListCount - 1
        If ListBox_PO.Selected(i) = False Then
            GoTo suite1
        Else
            For j = NrDebCal + 1 To nrowtotal
                If Cells(j, ncolCons).Value = ListBox_PO.List(i) Then
                    If Not IsNumeric(Cells(j, ncoJour).Value) Then
                        GoTo suite1
                    Else
                        Temps_travail_renseigne = Cells(j, ncoJour).Value
                        'on doit retirer la validation des données pour saisir notre nouvelle donnée puis la remettre ensuite
                        Cells(j, ncoJour).Select
                        With Selection.Validation
                            .Delete
                            .Add Type:=xlValidateInputOnly, AlertStyle:=xlValidAlertStop, Operator:=xlBetween
                            .IgnoreBlank = True
                            .InCellDropdown = True
                            .InputTitle = ""
                            .ErrorTitle = ""
                            .InputMessage = ""
                            .ErrorMessage = ""
                            .ShowInput = True
                            .ShowError = True
                        End With
                        Cells(j, ncoJour).Value = Round((((Temps_travail_renseigne * 7.4) - (temps_a_impacter / 60)) / 7.4), 2)
                        With Selection.Validation
                            .Delete
                            .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, Operator:=xlBetween, Formula1:="=CODES_X"
                            .IgnoreBlank = True
                            .InCellDropdown = True
                            .InputTitle = ""
                            .ErrorTitle = ""
                            .InputMessage = ""
                            .ErrorMessage = ""
                            .ShowInput = True
                            .ShowError = True
                        End With
                        GoTo suite1
                    End If
                End If
            Next j
        End If
suite1:
    Next i
    
    
    For Each MySheet In ActiveWorkbook.Worksheets
        MySheet.Activate
        MySheet.Protect DrawingObjects:=False, Contents:=True, Scenarios:= _
        True, AllowFormattingCells:=True, AllowFormattingColumns:=True, _
        AllowFormattingRows:=True, AllowSorting:=True, AllowFiltering:=True
    Next MySheet
        
    Sheets(UserForm_TempsPres.mois_concerne).Select
        
MsgBox "Vos modifications ont bien été effectuées!"

Unload Me
Unload UserForm_Initial

        
erreur1:
        
End Sub



