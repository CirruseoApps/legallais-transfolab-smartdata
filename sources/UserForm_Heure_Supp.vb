


Private Sub Quit_HS_Click()

Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

TextPO_HS.Value = ""
ComboBox_NBHS.Value = ""

    If Year(Date) = Annee_En_Cours Then
        DTPicker1_HS.Value = Date
        DTPicker2_HS.Value = Date
    Else
        If Year(Date) = Annee_En_Cours - 1 Then
            DTPicker1_HS.Value = DateSerial(Annee_En_Cours, 1, 1)
            DTPicker2_HS.Value = DateSerial(Annee_En_Cours, 1, 1)
        End If
    End If
Me.Hide

UserForm_Initial.OptionButton1.Value = False
UserForm_Initial.OptionButton2.Value = False
UserForm_Initial.OptionButton3.Value = False
UserForm_Initial.Show

End Sub

Private Sub UserForm_Initialize()
    
Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

    
    Me.ComboBox_NBHS.AddItem "15 min"
    Me.ComboBox_NBHS.AddItem "20 min"
    Me.ComboBox_NBHS.AddItem "30 min"
    Me.ComboBox_NBHS.AddItem "45 min"
    Me.ComboBox_NBHS.AddItem "1h"
    Me.ComboBox_NBHS.AddItem "1h15"
    Me.ComboBox_NBHS.AddItem "1h30"
    Me.ComboBox_NBHS.AddItem "1h45"
    Me.ComboBox_NBHS.AddItem "2h"
    Me.ComboBox_NBHS.AddItem "2h15"
    Me.ComboBox_NBHS.AddItem "2h30"
    Me.ComboBox_NBHS.AddItem "2h45"
    Me.ComboBox_NBHS.AddItem "3h"
    Me.ComboBox_NBHS.AddItem "3h15"
    Me.ComboBox_NBHS.AddItem "3h30"
    Me.ComboBox_NBHS.AddItem "3h45"
    Me.ComboBox_NBHS.AddItem "4h"
    
    
    'spécifie la date du jour lors de l'affichage de l'USF
    If Year(Date) = Annee_En_Cours Then
        DTPicker1_HS.Value = Date
        DTPicker2_HS.Value = Date
    Else
        If Year(Date) = Annee_En_Cours - 1 Then
            DTPicker1_HS.Value = DateSerial(Annee_En_Cours, 1, 1)
            DTPicker2_HS.Value = DateSerial(Annee_En_Cours, 1, 1)
        End If
    End If
   
    
    
End Sub


Private Sub DTPicker1_HS_Change()
    
'    If Year(DTPicker1_HS.Value) > Annee_En_Cours Then
'        DTPicker1_HS.Value = DateSerial(Annee_En_Cours, 12, 31)
'        DTPicker2_HS.Value = DateSerial(Annee_En_Cours, 12, 31)
'    Else
'        DTPicker2_HS.Value = DTPicker1_HS.Value
'    End If

End Sub

Private Sub DTPicker2_HS_Change()

 '   If DTPicker2_HS.Value < DTPicker1_HS.Value Then
 '       DTPicker2_HS.Value = DTPicker1_HS.Value
 '   End If

  '  If Year(DTPicker2_HS.Value) > Annee_En_Cours Then
  '      DTPicker2_HS.Value = DateSerial(Annee_En_Cours, 12, 31)
  '  End If

End Sub



Private Sub Valid_HS_Click()

Dim rngTrouve As Range
Dim PO_HS As Variant
Dim Mois1_HS As Variant, Mois2_HS As Variant, Jour1_HS As Date, Jour2_HS As Date, Lib_Mois1_HS As String, Lib_Mois2_HS As String
Dim NB_HS As String, Collaborateur_HS As String
Dim Nc_J1 As Long, Nc_J2 As Long
Dim Jour1_Cherch_HS As Date, Jour2_Cherch_HS As Date, NbJ1 As Variant, NbJ2 As Variant
Dim NcdernJour As Long
Dim Nb_Mois_HS As Long, NcPremJour As Long
Dim Lib_Mois_HS_Intercal As String
Dim NcoEqu As Variant, NcoPole As Variant
Dim Pole_HS As String, Equipe_HS As String



'on récupère le PO choisi
    PO_HS = Me.TextPO_HS.Value
    
'On récupère la date de début des heures suppl et la date de fin
Jour1_HS = DTPicker1_HS.Value
Jour2_HS = DTPicker2_HS.Value
Mois1_HS = Month(Jour1_HS)
Mois2_HS = Month(Jour2_HS)

If Mois1_HS >= 10 Then
    Jour1_Cherch_HS = "01/" & Mois1_HS & "/" & Annee_En_Cours
Else
    Jour1_Cherch_HS = "01/0" & Mois1_HS & "/" & Annee_En_Cours
End If

If Mois2_HS >= 10 Then
    Jour2_Cherch_HS = "01/" & Mois2_HS & "/" & Annee_En_Cours
Else
    Jour2_Cherch_HS = "01/0" & Mois2_HS & "/" & Annee_En_Cours
End If

NbJ1 = Jour1_HS - Jour1_Cherch_HS
NbJ2 = Jour2_HS - Jour2_Cherch_HS

Select Case Mois1_HS
    Case 1
        Lib_Mois1_HS = "Janvier " & Annee_En_Cours
    Case 2
        Lib_Mois1_HS = "Février " & Annee_En_Cours
    Case 3
        Lib_Mois1_HS = "Mars " & Annee_En_Cours
    Case 4
        Lib_Mois1_HS = "Avril " & Annee_En_Cours
    Case 5
        Lib_Mois1_HS = "Mai " & Annee_En_Cours
    Case 6
        Lib_Mois1_HS = "Juin " & Annee_En_Cours
    Case 7
        Lib_Mois1_HS = "Juillet " & Annee_En_Cours
    Case 8
        Lib_Mois1_HS = "Août " & Annee_En_Cours
    Case 9
        Lib_Mois1_HS = "Septembre " & Annee_En_Cours
    Case 10
        Lib_Mois1_HS = "Octobre " & Annee_En_Cours
    Case 11
        Lib_Mois1_HS = "Novembre " & Annee_En_Cours
    Case 12
        Lib_Mois1_HS = "Décembre " & Annee_En_Cours
End Select

Select Case Mois2_HS
    Case 1
        Lib_Mois2_HS = "Janvier " & Annee_En_Cours
    Case 2
        Lib_Mois2_HS = "Février " & Annee_En_Cours
    Case 3
        Lib_Mois2_HS = "Mars " & Annee_En_Cours
    Case 4
        Lib_Mois2_HS = "Avril " & Annee_En_Cours
    Case 5
        Lib_Mois2_HS = "Mai " & Annee_En_Cours
    Case 6
        Lib_Mois2_HS = "Juin " & Annee_En_Cours
    Case 7
        Lib_Mois2_HS = "Juillet " & Annee_En_Cours
    Case 8
        Lib_Mois2_HS = "Août " & Annee_En_Cours
    Case 9
        Lib_Mois2_HS = "Septembre " & Annee_En_Cours
    Case 10
        Lib_Mois2_HS = "Octobre " & Annee_En_Cours
    Case 11
        Lib_Mois2_HS = "Novembre " & Annee_En_Cours
    Case 12
        Lib_Mois2_HS = "Décembre " & Annee_En_Cours
End Select


        'on récupère le temps de réunion choisi
    Select Case Me.ComboBox_NBHS.Value
        Case "15 min"
            temps_a_impacter_HS = 15
        Case "20 min"
            temps_a_impacter_HS = 20
        Case "30 min"
            temps_a_impacter_HS = 30
        Case "45 min"
            temps_a_impacter_HS = 45
        Case "1h"
            temps_a_impacter_HS = 60
        Case "1h15"
            temps_a_impacter_HS = 75
        Case "1h30"
            temps_a_impacter_HS = 90
        Case "1h45"
            temps_a_impacter_HS = 105
        Case "2h"
            temps_a_impacter_HS = 120
        Case "2h15"
            temps_a_impacter_HS = 135
        Case "2h30"
            temps_a_impacter_HS = 150
        Case "2h45"
            temps_a_impacter_HS = 165
        Case "3h"
            temps_a_impacter_HS = 180
        Case "3h15"
            temps_a_impacter_HS = 195
        Case "3h30"
            temps_a_impacter_HS = 210
        Case "3h45"
            temps_a_impacter_HS = 225
        Case "4h"
            temps_a_impacter_HS = 240
        Case "4h15"
            temps_a_impacter_HS = 255
        Case "4h30"
            temps_a_impacter_HS = 270
        Case "4h45"
            temps_a_impacter_HS = 285
        Case "5h"
            temps_a_impacter_HS = 300
        Case "7h24"
            temps_a_impacter_HS = 444
    End Select
        

'On récupère le nombre d'heures supplémentaires
    NB_HS = temps_a_impacter_HS
    
'Travail en fonction du nombre de mois concerné
Nb_Mois_HS = Mois2_HS - Mois1_HS + 1



If NB_HS = "" Or TextPO_HS.Value = "" Then
    MsgBox "Attention! Vous avez omis de saisir des informations demandées. Merci de vérifier."
    GoTo Erreur_HS
End If



If Nb_Mois_HS = 1 Then
    ActiveWorkbook.Activate
        Sheets(Lib_Mois1_HS).Select
            Cells.Select
                Selection.Find(what:="Matricule").Activate
                    ncoMatricule = ActiveCell.Column
                    ncolCons = ActiveCell.Column + 1
                    NrDebCal = ActiveCell.Row
                    nrowtotal = ActiveSheet.UsedRange.Rows.Count
                    
                Selection.Find(what:="POLE").Activate
                    NcoPole = ActiveCell.Column
            
                Selection.Find(what:="EQUIPE").Activate
                    NcoEqu = ActiveCell.Column
            
            Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                Selection.Find(what:=DateValue(Jour1_Cherch_HS), LookIn:=xlFormulas).Activate
                    Nc_J1 = ActiveCell.Column + NbJ1

            Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                Selection.Find(what:=DateValue(Jour2_Cherch_HS), LookIn:=xlFormulas).Activate
                    Nc_J2 = ActiveCell.Column + NbJ2

                    
        ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_HS, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_HS.Value = ""
                    GoTo Erreur_HS
                End If
            
            Set rngTrouve = Nothing
        
        'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_HS).Select
                    Collaborateur_HS = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch_HS = ActiveCell.Row
                    
                    Pole_HS = Cells(Nr_Cherch_HS, NcoPole).Value
                    Equipe_HS = Cells(Nr_Cherch_HS, NcoEqu).Value
                    
        
        For i = Nc_J1 To Nc_J2
            If Cells(Nr_Cherch_HS, i).Value = "" Then
                Cells(Nr_Cherch_HS, i).Value = ""
            Else
                If IsNumeric(Cells(Nr_Cherch_HS, i).Value) Then

                    Cells(Nr_Cherch_HS, i).Select
                        Heure_renseigne = ActiveCell.Value
                       ActiveCell.Value = Round((((Heure_renseigne * 7.4) + (NB_HS / 60)) / 7.4), 2)
                End If
                    
            End If
        Next i
        
Else
    If Nb_Mois_HS = 2 Then
            ActiveWorkbook.Activate
            
            ''''''''''''''
            'on renseigne le mois 1 (de début des heures supp)
                Sheets(Lib_Mois1_HS).Select
                    Cells.Select
                        Selection.Find(what:="Matricule", lookat:=xlWhole).Activate
                            ncoMatricule = ActiveCell.Column
                            ncolCons = ActiveCell.Column + 1
                            NrDebCal = ActiveCell.Row
                            nrowtotal = ActiveSheet.UsedRange.Rows.Count
                    Cells.Select
                        Selection.Find(what:="Nb Jours Travaillés", lookat:=xlWhole).Select
                            NcdernJour = ActiveCell.Column - 1

                    Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                        Selection.Find(what:=DateValue(Jour1_Cherch), LookIn:=xlFormulas).Activate
                            Nc_J1 = ActiveCell.Column + NbJ1
        
                    
            ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_HS, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_HS.Value = ""
                    GoTo Erreur_HS
                End If
            
            Set rngTrouve = Nothing
        
            'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_HS).Select
                    Collaborateur_HS = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch_HS = ActiveCell.Row
                    
                    Pole_HS = Cells(Nr_Cherch_HS, NcoPole).Value
                    Equipe_HS = Cells(Nr_Cherch_HS, NcoEqu).Value
                    
        
                For i = Nc_J1 To NcdernJour
                    If Cells(Nr_Cherch_HS, i).Value = "" Then
                        Cells(Nr_Cherch_HS, i).Value = ""
                    Else
                        If IsNumeric(Cells(Nr_Cherch_HS, i).Value) Then
        
                            Cells(Nr_Cherch_HS, i).Select
                                Heure_renseigne = ActiveCell.Value
                               ActiveCell.Value = Round((((Heure_renseigne * 7.4) + (NB_HS / 60)) / 7.4), 2)
                        End If
                            
                    End If
                Next i



        
        ''''''''''''''
            'on renseigne le mois 2 (de fin d'heures supplémentaires)
                Sheets(Lib_Mois2_HS).Select
                    Cells.Select
                        Selection.Find(what:="Matricule", lookat:=xlWhole).Activate
                            ncoMatricule = ActiveCell.Column
                            ncolCons = ActiveCell.Column + 1
                            NrDebCal = ActiveCell.Row
                            nrowtotal = ActiveSheet.UsedRange.Rows.Count
                            
                        Selection.Find(what:="POLE").Activate
                            NcoPole = ActiveCell.Column
                    
                        Selection.Find(what:="EQUIPE").Activate
                            NcoEqu = ActiveCell.Column

                    Cells.Select
                        Selection.Find(what:="FONCTION", lookat:=xlWhole).Select
                            NcPremJour = ActiveCell.Column + 1

                    Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                        Selection.Find(what:=DateValue(Jour2_Cherch), LookIn:=xlFormulas).Activate
                            Nc_J2 = ActiveCell.Column + NbJ2
        
                    
        ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_HS, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_HS.Value = ""
                    GoTo Erreur_HS
                End If
            
            Set rngTrouve = Nothing
        
        'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_HS).Select
                    Collaborateur_HS = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch_HS = ActiveCell.Row
                    
                    Pole_HS = Cells(Nr_Cherch_HS, NcoPole).Value
                    Equipe_HS = Cells(Nr_Cherch_HS, NcoEqu).Value
                    
        
        For i = NcPremJour To Nc_J2
                    If Cells(Nr_Cherch_HS, i).Value = "" Then
                        Cells(Nr_Cherch_HS, i).Value = ""
                    Else
                        If IsNumeric(Cells(Nr_Cherch_HS, i).Value) Then
        
                            Cells(Nr_Cherch_HS, i).Select
                                Heure_renseigne = ActiveCell.Value
                               ActiveCell.Value = Round((((Heure_renseigne * 7.4) + (NB_HS / 60)) / 7.4), 2)
                        End If
                            
                    End If
                Next i

    Else
        If Nb_Mois_HS > 2 Then
        
            ActiveWorkbook.Activate
            
            ''''''''''''''
            'on renseigne le mois 1 (de début d'heures supplémentaires)
                Sheets(Lib_Mois1_HS).Select
                    Cells.Select
                        Selection.Find(what:="Matricule", lookat:=xlWhole).Activate
                            ncoMatricule = ActiveCell.Column
                            ncolCons = ActiveCell.Column + 1
                            NrDebCal = ActiveCell.Row
                            nrowtotal = ActiveSheet.UsedRange.Rows.Count
                            
                        Selection.Find(what:="POLE").Activate
                            NcoPole = ActiveCell.Column
                    
                        Selection.Find(what:="EQUIPE").Activate
                            NcoEqu = ActiveCell.Column

                    Cells.Select
                        Selection.Find(what:="Nb Jours Travaillés", lookat:=xlWhole).Select
                            NcdernJour = ActiveCell.Column - 1

                    Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                        Selection.Find(what:=DateValue(Jour1_Cherch), LookIn:=xlFormulas).Activate
                            Nc_J1 = ActiveCell.Column + NbJ1
        
                    
            ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_HS, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_HS.Value = ""
                    GoTo Erreur_HS
                End If
            
            Set rngTrouve = Nothing
        
            'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_HS).Select
                    Collaborateur_HS = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch_HS = ActiveCell.Row
                    
                    Pole_HS = Cells(Nr_Cherch_HS, NcoPole).Value
                    Equipe_HS = Cells(Nr_Cherch_HS, NcoEqu).Value
                    
        
                For i = Nc_J1 To NcdernJour
                    If Cells(Nr_Cherch_HS, i).Value = "" Then
                        Cells(Nr_Cherch_HS, i).Value = ""
                    Else
                        If IsNumeric(Cells(Nr_Cherch_HS, i).Value) Then
        
                            Cells(Nr_Cherch_HS, i).Select
                                Heure_renseigne = ActiveCell.Value
                               ActiveCell.Value = Round((((Heure_renseigne * 7.4) + (NB_HS / 60)) / 7.4), 2)
                        End If
                            
                    End If
                Next i

        
        ''''''''''''''
            'on renseigne le dernier mois (de fin d'heures supplémentaires)
                Sheets(Lib_Mois2_HS).Select
                    Cells.Select
                        Selection.Find(what:="Matricule", lookat:=xlWhole).Activate
                            ncoMatricule = ActiveCell.Column
                            ncolCons = ActiveCell.Column + 1
                            NrDebCal = ActiveCell.Row
                            nrowtotal = ActiveSheet.UsedRange.Rows.Count
                        
                        Selection.Find(what:="POLE").Activate
                            NcoPole = ActiveCell.Column
                    
                        Selection.Find(what:="EQUIPE").Activate
                            NcoEqu = ActiveCell.Column

                            
                    Cells.Select
                        Selection.Find(what:="FONCTION", lookat:=xlWhole).Select
                            NcPremJour = ActiveCell.Column + 1

                    Rows("" & NrDebCal & ":" & NrDebCal & "").Select
                        Selection.Find(what:=DateValue(Jour2_Cherch), LookIn:=xlFormulas).Activate
                            Nc_J2 = ActiveCell.Column + NbJ2
        
                    
        ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_HS, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_HS.Value = ""
                    GoTo Erreur_HS
                End If
            
            Set rngTrouve = Nothing
        
        'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_HS).Select
                    Collaborateur_HS = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch_HS = ActiveCell.Row
                    
                    Pole_HS = Cells(Nr_Cherch_HS, NcoPole).Value
                    Equipe_HS = Cells(Nr_Cherch_HS, NcoEqu).Value
                    
        
                For i = NcPremJour To Nc_J2
                    If Cells(Nr_Cherch_HS, i).Value = "" Then
                        Cells(Nr_Cherch_HS, i).Value = ""
                    Else
                        If IsNumeric(Cells(Nr_Cherch_HS, i).Value) Then
        
                            Cells(Nr_Cherch_HS, i).Select
                                Heure_renseigne = ActiveCell.Value
                               ActiveCell.Value = Round((((Heure_renseigne * 7.4) + (NB_HS / 60)) / 7.4), 2)
                        End If
                            
                    End If
                Next i


        ''''''''''''''
            'on renseigne les mois intercalés
            For j = Mois1_HS + 1 To Mois2_HS - 1
                
                Select Case j
                    Case 1
                        Lib_Mois_HS_Intercal = "Janvier " & Annee_En_Cours
                    Case 2
                        Lib_Mois_HS_Intercal = "Février " & Annee_En_Cours
                    Case 3
                        Lib_Mois_HS_Intercal = "Mars " & Annee_En_Cours
                    Case 4
                        Lib_Mois_HS_Intercal = "Avril " & Annee_En_Cours
                    Case 5
                        Lib_Mois_HS_Intercal = "Mai " & Annee_En_Cours
                    Case 6
                        Lib_Mois_HS_Intercal = "Juin " & Annee_En_Cours
                    Case 7
                        Lib_Mois_HS_Intercal = "Juillet " & Annee_En_Cours
                    Case 8
                        Lib_Mois_HS_Intercal = "Août " & Annee_En_Cours
                    Case 9
                        Lib_Mois_HS_Intercal = "Septembre " & Annee_En_Cours
                    Case 10
                        Lib_Mois_HS_Intercal = "Octobre " & Annee_En_Cours
                    Case 11
                        Lib_Mois_HS_Intercal = "Novembre " & Annee_En_Cours
                    Case 12
                        Lib_Mois_HS_Intercal = "Décembre " & Annee_En_Cours
                End Select

                
                
                Sheets(Lib_Mois_HS_Intercal).Select
                    Cells.Select
                        Selection.Find(what:="Matricule", lookat:=xlWhole).Activate
                            ncoMatricule = ActiveCell.Column
                            ncolCons = ActiveCell.Column + 1
                            NrDebCal = ActiveCell.Row
                            nrowtotal = ActiveSheet.UsedRange.Rows.Count
                    
                        Selection.Find(what:="POLE").Activate
                            NcoPole = ActiveCell.Column
                    
                        Selection.Find(what:="EQUIPE").Activate
                            NcoEqu = ActiveCell.Column

                    Cells.Select
                        Selection.Find(what:="FONCTION", lookat:=xlWhole).Select
                            NcPremJour = ActiveCell.Column + 1

                    Cells.Select
                        Selection.Find(what:="Nb Jours Travaillés", lookat:=xlWhole).Select
                            NcdernJour = ActiveCell.Column - 1
        
                    
        ' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_HS, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO_HS.Value = ""
                    GoTo Erreur_HS
                End If
            
            Set rngTrouve = Nothing
        
        'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_HS).Select
                    Collaborateur_HS = Cells(ActiveCell.Row, ncolCons).Value
                    Nr_Cherch_HS = ActiveCell.Row
                    
                    Pole_HS = Cells(Nr_Cherch_HS, NcoPole).Value
                    Equipe_HS = Cells(Nr_Cherch_HS, NcoEqu).Value
                    
        
                For i = NcPremJour To NcdernJour
                    If Cells(Nr_Cherch_HS, i).Value = "" Then
                        Cells(Nr_Cherch_HS, i).Value = ""
                    Else
                        If IsNumeric(Cells(Nr_Cherch_HS, i).Value) Then
        
                            Cells(Nr_Cherch_HS, i).Select
                                Heure_renseigne = ActiveCell.Value
                               ActiveCell.Value = Round((((Heure_renseigne * 7.4) + (NB_HS / 60)) / 7.4), 2)
                        End If
                            
                    End If
                Next i


            Next j

        End If
    End If
End If

Sheets(Lib_Mois1_HS).Select


'''''''''''''''''''''''''''''''''''''''''''''
'On renseigne l'onglet Heures supplémentaires
'''''''''''''''''''''''''''''''''''''''''''''
Dim Nc_Pole_Synth As Variant, Nc_Equipe_Synth As Variant, Nc_PO_Synth As Variant
Dim Nc_Nom_Synth As Variant, Nc_Date_Deb_Synth As Variant, Nc_Date_Fin_Synth As Variant, Nc_Nb_HS_Synth As Variant

Sheets("Heures supplémentaires").Select
    ActiveSheet.Unprotect Password:="ziggy"

    Rows("1:1").Select
        Selection.Find(what:="Pôle", lookat:=xlWhole).Select
            Nc_Pole_Synth = ActiveCell.Column
    Rows("1:1").Select
        Selection.Find(what:="Equipe", lookat:=xlWhole).Select
            Nc_Equipe_Synth = ActiveCell.Column
    Rows("1:1").Select
        Selection.Find(what:="PO", lookat:=xlWhole).Select
            Nc_PO_Synth = ActiveCell.Column
    Rows("1:1").Select
        Selection.Find(what:="Nom", lookat:=xlWhole).Select
            Nc_Nom_Synth = ActiveCell.Column
    Rows("1:1").Select
        Selection.Find(what:="Date Début", lookat:=xlWhole).Select
            Nc_Date_Deb_Synth = ActiveCell.Column
    Rows("1:1").Select
        Selection.Find(what:="Date Fin", lookat:=xlWhole).Select
            Nc_Date_Fin_Synth = ActiveCell.Column
    Rows("1:1").Select
        Selection.Find(what:="Nombre Heures Suppl.", lookat:=xlWhole).Select
            Nc_Nb_HS_Synth = ActiveCell.Column
            
    
    Nr_Cible = Cells(10000, 1).End(xlUp).Offset(1, 0).Row
    
    Cells(Nr_Cible, Nc_Pole_Synth).Value = Pole_HS
    Cells(Nr_Cible, Nc_Equipe_Synth).Value = Equipe_HS
    Cells(Nr_Cible, Nc_PO_Synth).Value = PO_HS
    Cells(Nr_Cible, Nc_Nom_Synth).Value = Collaborateur_HS
    Cells(Nr_Cible, Nc_Date_Deb_Synth).Value = Jour1_HS
    Cells(Nr_Cible, Nc_Date_Fin_Synth).Value = Jour2_HS
    Cells(Nr_Cible, Nc_Nb_HS_Synth).Value = NB_HS
            
    
            

'remise à vide de l'userform
TextPO_HS.Value = ""
ComboBox_NBHS.Value = ""

    If Year(Date) = Annee_En_Cours Then
        DTPicker1_HS.Value = Date
        DTPicker2_HS.Value = Date
    Else
        DTPicker1_HS.Value = DateSerial(Annee_En_Cours, 1, 1)
        DTPicker2_HS.Value = DateSerial(Annee_En_Cours, 1, 1)
    End If


UserForm_Heure_Supp.Hide



Erreur_HS:
End Sub
