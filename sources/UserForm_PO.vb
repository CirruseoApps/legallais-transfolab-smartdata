
Public ncoMatricule As Integer  'N° de la colonne Matricule
Public ncolCons As Integer     'N° de la colonne Conseiller
Public NrDebCal As Integer     'N° de la 1ere ligne du calendrier
Public nrowtotal As Integer    'Nombre de lignes du calendrier
Public PO_concerne As String
Public collaborateur_concerne As String
Public temps_a_impacter As Variant
Public plage_PO As String
Public plage_CQP As String
Public plage_Tps As String
Public plage_Mois As String
Public plage_Annee As String
Public temps_tutorat_mensuel As Variant
Public temps_mensuel_Formation_occasionnelle As Variant
Public temps_accompagnement As Variant
Public mois As Integer
Public Annee_En_Cours As Variant



Private Sub Quit2_Click()
TextPO.Value = ""
ComboBox_temps_reunion.Value = ""

Unload Me
UserForm_TempsPres.DTPicker1 = Date
UserForm_TempsPres.OptionButton1.Value = False
UserForm_TempsPres.OptionButton2.Value = False
UserForm_TempsPres.Show
End Sub

Private Sub UserForm_Initialize()
    

Annee_En_Cours = Val(Right(Left(ActiveWorkbook.Name, Len(ActiveWorkbook.Name) - 5), 4))

    
    Me.ComboBox_temps_reunion.AddItem "15 min"
    Me.ComboBox_temps_reunion.AddItem "20 min"
    Me.ComboBox_temps_reunion.AddItem "30 min"
    Me.ComboBox_temps_reunion.AddItem "45 min"
    Me.ComboBox_temps_reunion.AddItem "1h"
    Me.ComboBox_temps_reunion.AddItem "1h15"
    Me.ComboBox_temps_reunion.AddItem "1h30"
    Me.ComboBox_temps_reunion.AddItem "1h45"
    Me.ComboBox_temps_reunion.AddItem "2h"
    Me.ComboBox_temps_reunion.AddItem "2h15"
    Me.ComboBox_temps_reunion.AddItem "2h30"
    Me.ComboBox_temps_reunion.AddItem "2h45"
    Me.ComboBox_temps_reunion.AddItem "3h"
    Me.ComboBox_temps_reunion.AddItem "3h15"
    Me.ComboBox_temps_reunion.AddItem "3h30"
    Me.ComboBox_temps_reunion.AddItem "3h45"
    Me.ComboBox_temps_reunion.AddItem "4h"
    Me.ComboBox_temps_reunion.AddItem "4h15"
    Me.ComboBox_temps_reunion.AddItem "4h30"
    Me.ComboBox_temps_reunion.AddItem "4h45"
    Me.ComboBox_temps_reunion.AddItem "5h"
    Me.ComboBox_temps_reunion.AddItem "7h24"
    
End Sub

'empêche la saisie d'autre chose que des chiffres
Private Sub TextPO_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    If InStr("1234567890", Chr(KeyAscii)) = 0 Then KeyAscii = 0
End Sub


Private Sub Valid2_Click()

Dim rngTrouve As Range
Dim text As Variant



If ComboBox_temps_reunion.Value = "" Or TextPO.Value = "" Then
    MsgBox "Attention! Vous avez omis de saisir des informations demandées. Merci de vérifier."
    GoTo erreur1
End If


'on récupère le PO choisi
    PO_concerne = Me.TextPO.Value
    
    Windows("Congés " & Annee_En_Cours & ".xlsm").Activate
        Sheets(UserForm_TempsPres.mois_concerne).Select
            Cells.Select
                Selection.Find(what:="Matricule").Activate
                    ncoMatricule = ActiveCell.Column
                    ncolCons = ActiveCell.Column + 1
                    NrDebCal = ActiveCell.Row
                    nrowtotal = ActiveSheet.UsedRange.Rows.Count

                    
' on controle la validité du PO demandé
            Set rngTrouve = ActiveSheet.Columns(ncoMatricule).Cells.Find(what:=PO_concerne, LookIn:=xlValues, lookat:=xlWhole)

                If rngTrouve Is Nothing Then
                    MsgBox "Le PO que vous avez saisi n'a pas été trouvé. Merci de saisir à nouveau le PO."
                    Me.TextPO.Value = ""
                    GoTo erreur1
                End If
            
            Set rngTrouve = Nothing
        
'on identifie le collaborateur choisi
            Range(Cells(NrDebCal, ncoMatricule), Cells(nrowtotal, ncoMatricule)).Select
                Selection.Find(what:=PO_concerne).Select
                    collaborateur_concerne = Cells(ActiveCell.Row, ncolCons).Value

 
 
'on récupère le temps de réunion choisi
    Select Case Me.ComboBox_temps_reunion.Value
        Case "15 min"
            temps_a_impacter = 15
        Case "20 min"
            temps_a_impacter = 20
        Case "30 min"
            temps_a_impacter = 30
        Case "45 min"
            temps_a_impacter = 45
        Case "1h"
            temps_a_impacter = 60
        Case "1h15"
            temps_a_impacter = 75
        Case "1h30"
            temps_a_impacter = 90
        Case "1h45"
            temps_a_impacter = 105
        Case "2h"
            temps_a_impacter = 120
        Case "2h15"
            temps_a_impacter = 135
        Case "2h30"
            temps_a_impacter = 150
        Case "2h45"
            temps_a_impacter = 165
        Case "3h"
            temps_a_impacter = 180
        Case "3h15"
            temps_a_impacter = 195
        Case "3h30"
            temps_a_impacter = 210
        Case "3h45"
            temps_a_impacter = 225
        Case "4h"
            temps_a_impacter = 240
        Case "4h15"
            temps_a_impacter = 255
        Case "4h30"
            temps_a_impacter = 270
        Case "4h45"
            temps_a_impacter = 285
        Case "5h"
            temps_a_impacter = 300
        Case "7h24"
            temps_a_impacter = 444
    End Select
    
    
    
    
    UserForm_PO.Hide
    
    UserForm_Message.Show

erreur1:

End Sub

